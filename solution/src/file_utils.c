#include "file_utils.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

int open_file(const char *restrict __filename, const char *restrict __modes, FILE **in) {
    *in = fopen(__filename, __modes);
    return errno;
}

int close_file(FILE *__stream) {
    int error = fclose(__stream);
    return error;
}

void print_file_open_error(int error, char const *fname, FILE *out) {
    fprintf(out, "Something went wrong when trying to open file '%s':\n%s\n", fname,
            strerror(error));
}

void print_file_close_error(int error, char const *fname, FILE *out) {
    fprintf(out, "Something went wrong when trying to open file '%s':\n%s\n", fname,
            strerror(error));
}
