#include "image.h"

#include <stdlib.h>

struct image rotate(struct image const source) {
    struct pixel *rotated_data = malloc(sizeof(struct pixel) * source.width * source.height);

    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotated_data[(source.width - j - 1) * source.height + i] =
                source.data[i * source.width + j];
        }
    }

    struct image rotatedImg = create_image(source.height, source.width, rotated_data);

    return rotatedImg;
}
