#include <errno.h>
#include <stdint.h>
#include <stdio.h>

#include "file_utils.h"
#include "image.h"
#include "image_io.h"
#include "image_utils.h"

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr,
                "Program uses two arguments, but ony %d were specified\n"
                "Usage: image-transformer input-file output-file\n",
                argc - 1);
        return 1;
    }

    int error;
    struct image img_in = {0};

    FILE *in;
    error = open_file(argv[1], "r", &in);
    if (error) {
        print_file_open_error(error, argv[1], stdout);
        return error;
    }

    enum read_status r_status = read_image(BMP, in, &img_in);
    if (r_status) {
        print_img_read_error(r_status, stderr);
        return 1;
    }

    error = close_file(in);
    if (error) {
        destroy_image(img_in);
        print_file_close_error(error, argv[1], stdout);
        return error;
    }

    struct image img_out = rotate(img_in);
    destroy_image(img_in);

    FILE *out;
    error = open_file(argv[2], "w", &out);
    if (error) {
        print_file_open_error(error, argv[2], stdout);
        return error;
    }

    enum write_status w_status = write_image(BMP, out, &img_out);
    destroy_image(img_out);
    if (w_status) {
        print_img_write_error(w_status, stderr);
        return 1;
    }

    error = close_file(out);
    if (error) {
        print_file_close_error(error, argv[2], stdout);
        return error;
    }

    return 0;
}
