#include "bmp.h"
#include "image.h"
#include "image_io.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_BF_TYPE 0x4d42
#define BMP_FILE_HEADER_SIZE 14
#define BMP_RESERVED 0
#define BMP_OFF_BITS (BMP_FILE_HEADER_SIZE + BMP_INFO_SIZE)
#define BMP_INFO_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0
#define X_PELS_PER_METER 2834
#define Y_PELS_PER_METER 2834

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool check_bmp_head_validity(const struct bmp_header *header);
static uint8_t calc_bmp_str_padding(size_t width) { return 4 - (width * 3) % 4; }

static size_t calc_bmp_str_data_size(size_t width, size_t height) {
    uint8_t padding = calc_bmp_str_padding(width);
    return (width * 3 + padding) * height;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    size_t items_read;
    struct bmp_header header = {0};
    items_read = fread(&header, sizeof(struct bmp_header), 1, in);

    if (items_read != 1) { return READ_INVALID_BITS; }
    if (header.bfType != BMP_BF_TYPE) { return READ_INVALID_SIGNATURE; }
    if (!check_bmp_head_validity(&header)) { return READ_INVALID_HEADER; }

    uint8_t padding = calc_bmp_str_padding(header.biWidth);

    struct pixel *data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);

    *img = create_image(header.biWidth, header.biHeight, data);

    for (size_t i = header.biHeight; i != 0; i--) {
        items_read = fread(img->data + (i - 1) * header.biWidth,
                           sizeof(struct pixel) * header.biWidth, 1, in);

        if (items_read != 1) {
            free(img->data);
            return READ_INVALID_BITS;
        }

        if (fseek(in, +padding, SEEK_CUR) == -1) {
            free(img->data);
            return READ_INVALID_BITS;
        };
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    uint8_t padding = calc_bmp_str_padding(img->width);
    size_t data_size = calc_bmp_str_data_size(img->width, img->height);
    struct bmp_header header = {.bfType = BMP_BF_TYPE,
                                .bfSize = data_size + BMP_OFF_BITS,
                                .bfReserved = BMP_RESERVED,
                                .bfOffBits = BMP_OFF_BITS,
                                .biSize = BMP_INFO_SIZE,
                                .biWidth = img->width,
                                .biHeight = img->height,
                                .biPlanes = BMP_PLANES,
                                .biBitCount = BMP_BIT_COUNT,
                                .biCompression = BMP_COMPRESSION,
                                .biSizeImage = data_size,
                                .biXPelsPerMeter = X_PELS_PER_METER,
                                .biYPelsPerMeter = Y_PELS_PER_METER,
                                .biClrUsed = BMP_CLR_USED,
                                .biClrImportant = BMP_CLR_IMPORTANT};

    size_t items_written;

    items_written = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (items_written != 1) { return WRITE_ERROR; }

    // for each "string" of image data (pixels) and padding are written
    for (size_t i = header.biHeight; i != 0; i--) {
        items_written = fwrite(img->data + (i - 1) * header.biWidth,
                               sizeof(struct pixel) * header.biWidth, 1, out);

        if (items_written != 1) { return WRITE_ERROR; }

        if (fseek(out, padding, SEEK_CUR) == -1) { return WRITE_ERROR; };
    }

    return WRITE_OK;
}

static bool check_bmp_head_validity(const struct bmp_header *header) {
    if (header->bfType != BMP_BF_TYPE) { return false; }

    size_t data_size = calc_bmp_str_data_size(header->biWidth, header->biHeight);

    // bfSize and bfSizeImage are compared by less than
    // because trailing zeros in data are somehow acceptable
    if (header->bfSize < data_size + BMP_OFF_BITS) { return false; }
    if (header->biSizeImage < data_size) { return false; }
    if (header->bfReserved != BMP_RESERVED) { return false; }
    if (header->bfOffBits != BMP_OFF_BITS) { return false; }
    if (header->biSize != BMP_INFO_SIZE) { return false; }
    if (header->biPlanes != BMP_PLANES) { return false; }
    if (header->biBitCount != BMP_BIT_COUNT) { return false; }
    if (header->biCompression != BMP_COMPRESSION) { return false; }
    if (header->biXPelsPerMeter != X_PELS_PER_METER) { return false; }
    if (header->biYPelsPerMeter != Y_PELS_PER_METER) { return false; }
    if (header->biClrUsed != BMP_CLR_USED) { return false; }
    if (header->biClrImportant != BMP_CLR_IMPORTANT) { return false; }

    return true;
}
