#include "image_io.h"
#include "bmp.h"

enum read_status read_image(enum image_types type, FILE *in, struct image *img) {
    enum read_status (*readers[])(FILE *, struct image *) = {[BMP] = from_bmp};

    return readers[type](in, img);
}

enum write_status write_image(enum image_types type, FILE *out, const struct image *img) {
    enum write_status (*writters[])(FILE *, const struct image *) = {[BMP] = to_bmp};

    return writters[type](out, img);
}

void print_img_read_error(enum read_status status, FILE *out) {
    static const char *read_err_msg[] = {[READ_INVALID_SIGNATURE] = ": Invalid file signature",
                                         [READ_INVALID_BITS] = ": Invalid data",
                                         [READ_INVALID_HEADER] = ": Invalid file header"};

    if (status != READ_OK) {
        fprintf(out, "An error occured when trying to read file%s\n", read_err_msg[status]);
    }
}

void print_img_write_error(enum write_status status, FILE *out) {
    static const char *write_err_msg[] = {
        [WRITE_ERROR] = "", [WRITE_IVALID_FILE_TYPE] = ": Invalid file type"};

    if (status != WRITE_OK) {
        fprintf(out, "An error occured when trying to write file%s\n", write_err_msg[status]);
    }
}
