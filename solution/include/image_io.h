#ifndef _IMAGE_IO_H
#define _IMAGE_IO_H

#include "image.h"
#include <stdio.h>

enum image_types { BMP };

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_IVALID_FILE_TYPE,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_IVALID_FILE_TYPE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status read_image(enum image_types type, FILE *in, struct image *img);

enum write_status write_image(enum image_types type, FILE *out, const struct image *img);

void print_img_read_error(enum read_status status, FILE *out);

void print_img_write_error(enum write_status status, FILE *out);

#endif
