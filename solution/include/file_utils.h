#ifndef _FILE_UTILS_H
#define _FILE_UTILS_H

#include <stdint.h>
#include <stdio.h>

int open_file(const char *restrict __filename, const char *restrict __modes, FILE **in);

int close_file(FILE *__stream);

void print_file_open_error(int error, char const *fname, FILE *out);

void print_file_close_error(int error, char const *fname, FILE *out);

#endif
