#ifndef _IMAGE_UTILS_H
#define _IMAGE_UTILS_H

#include "image.h"

struct image (*image_transformation)(struct image const source);

struct image rotate(struct image const source);

#endif
