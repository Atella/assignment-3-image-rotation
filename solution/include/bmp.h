#ifndef _BMH_H
#define _BMH_H

#include "image_io.h"

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

#endif
